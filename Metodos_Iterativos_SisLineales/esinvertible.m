function [ sal ] = esinvertible( A )
%Esta funcion calcula si una matriz es invertible o no

sal=0;
[n,m]=size(A);

if(n==m)
    if(det(A)~=0)
        sal=1;
    end
end

