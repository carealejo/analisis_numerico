function [ x ] = gauss_seidel( A,b,xo,err )
%si al matriz A tiene inversa ( si el det es diferente de 0)
%(si la amtriz es cuadrada)

[n,m]=size(A);

D=zeros(n);
L=zeros(n);
U=zeros(n);

if(esinvertible(A)==1)
    %disp('es posible');
    for(i=1:1:n)
        for(j=1:1:n)
            if(i==j)
                D(i,j)=A(i,j);
            end
            if(i>j)               
                L(i,j)=A(i,j);                  
            end            
            if(i<j)            
                U(i,j)=A(i,j);                  
            end            
        end        
    end    
    if(esinvertible(D+L))
        M=-inv(D+L)*(U);
        M2=inv(D+L)*b;
        x=(M*xo)+(M2);
        if((norm((x-xo),inf)/norm(x,inf))>err)
            x=jacobi(A,b,x,err)
        end
    else
        disp('la nueva D no es invertible');
    end
end


end
