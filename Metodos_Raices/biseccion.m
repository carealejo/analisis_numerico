function [ sal ] = biseccion(a,b,tol,N,sel)
%Metodo de biseccion:
%la funcion F debe ser continua en el intervalo [a,b]
%el teorema del valor medio garantiza que existe un F(x)=0
%es decir un corte en el eje x

    if sel==1
        x = linspace(a,b,200);
        y = funcion(x);
        plot(x,y);
        sal=0;
    else
        c=(a+b)/2;
        if(tol > abs(b-a) || N==0)
            sal=c;
            disp(200-N);
        elseif((funcion(a)*funcion(c))<0)
            sal=biseccion(a,c,tol,N-1,0);
        elseif((funcion(c)*funcion(b))<0)
            sal=biseccion(c,b,tol,N-1,0);
        elseif(funcion(c)==0)
            disp(200-N);
            sal=c;
        end
    end
end