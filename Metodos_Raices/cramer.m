function x = cramer(A,b)
%Esta funcion calcula Ax*b por medio de la regla de cramer

[m,n]=size(A);

for i=1:1:n

    B=A;
    
    B(:,i)=b;
    
    x(i)=(det(B)/det(A));

end

