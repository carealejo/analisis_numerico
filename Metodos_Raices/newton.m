function [ sal ] = newton(a,b,xo,tol)
%Metodo de Newton:
%F debe ser diferenciable en un intervalo [a,b]
%No deben haber puntos de infleccion

    c=((-funcion(xo))/(funcionp(xo)))+xo;
    
    if(tol>abs(funcion(xo)))
        sal=c;
    else
        sal=newton(a,b,c,tol); 
    end
    
end

