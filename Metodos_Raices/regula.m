function [ sal, salcont ] = regula(a,b,tol,N,sel)
%Metodo de regula_falsi:
%la funcion F debe ser continua en el intervalo [a,b]
%el teorema del valor medio garantiza que existe un F(x)=0
%es decir un corte en el eje x

    if sel==1
        x = linspace(a,b,200);
        y = funcion(x);
        plot(x,y);
        sal=0;
    else
        m=(funcion(b)-funcion(a))/(b-a);
        c=((-funcion(a)*(b-a))/(funcion(b)-funcion(a)))+a;
        if(tol > abs(funcion(c)) || N==0)
            sal=c;
            disp(200-N);
        elseif((funcion(a)*funcion(c))<0)
            sal=regula(a,c,tol,N-1,0);
        elseif((funcion(c)*funcion(b))<0)
            sal=regula(c,b,tol,N-1,0);
        elseif(funcion(c)==0)
            sal=c; 
            disp(200-N);
        end
    end
    
end