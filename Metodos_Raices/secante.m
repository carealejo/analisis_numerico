function [ sal ] = secante(a,b,xo,xi,tol)
%Metodo de Secante:
%F debe ser diferenciable en un intervalo [a,b]
%No deben haber puntos de infleccion

    if(tol>funcion(xi))
        sal=xi;
    else
        xn=xi-(((xi-xo)/(funcion(xi)-funcion(xo)))*funcion(xi));
        sal=secante(a,b,xi,xn,tol);
    end
  
end

