function x = triinf(A,b)
%Esta funcion calcula la solucion de Ax=b
%para una matriz con triangulacion inferior
%sea A una matriz tal que si j>i Aij=0
%y un vector bER^n, A de tamaño n*n

[m,n]=size(A);
x(1)=b(1)/A(1,1);

for i=2:1:n
   
    sum=0;
    
    for j=1:1:i-1
       
        sum=sum+A(i,j)*x(j);
        
    end
    
    x(i)=(b(i)- sum)/A(i,i);
    
end


end

