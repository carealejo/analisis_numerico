function x = trisup(A,b)
%Esta funcion calcula la solucion a una matriz triangular
%Sea A una matriz tal que si i>j Aij=0 y un vector bER^n 
%A de tamaño n*n.

    [m,n]=size(A);
    
    x(n)=b(n)/A(n,n);

    for i=n-1:-1:1
        
        sum=0;
        
        for j=i+1:n
            
            sum=sum+A(i,j)*x(j);
            
        end
        
        x(i)=(b(i)-sum)/A(i,i);
        
    end
    
end
