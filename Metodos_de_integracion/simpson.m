function [ Y ] = simpson(a,b,n)

    %Esta funcion calcula la integral por el metodo de simpson
    %Si se quiere usar el metodo compuesto se pone el parametro
    % n en el valor de las particiones si no se debe poner en 1 para
    %usar el metodo normal, es decir, no compuesto.

    h=(b-a)/n;
    iter=0;
    cont=func(a)+func(b);
    
    for i=a+h:h:b-h
       if (mod(iter,2)==0)  
           cont=cont+4*(func(i)); 
       else
          cont=cont+2*(func(i));       
       end
       iter=iter+1;
    end
    
    Y=(h/3)*cont;

end