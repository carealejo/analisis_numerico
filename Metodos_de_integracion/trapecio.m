function [ Y ] = trapecio(a,b,n)

    %Esta funcion calcula la integral por el metodo del trapecio
    %Si se quiere usar el metodo compuesto se pone el parametro
    % n en el valor de las particiones si no se debe poner en 1 para
    %usar el metodo normal, es decir, no compuesto.

    h=(b-a)/n;
    
    cont=(func(a));
    
    for i=a+h:h:b-h
        cont=cont+(2*(func(i)));
    end
    
    cont=cont+(func(b));
    
    Y=(h/2)*cont;

end