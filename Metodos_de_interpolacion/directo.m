function [ x ] = directo( p )

[n,m]=size(p);

A=zeros(n);
b=zeros(n,1);
xo=zeros(n,1);

for(i=1:1:n)
    for(j=1:1:n)
        if(j==1)
           A(i,j)=1;
        else
           A(i,j)=(p(i,1)^(j-1)); 
        end
     end
end


for(j=1:1:n)
   b(j,1)=p(j,2);
end

disp(b);
disp(A);
disp(xo);

jacobi(A,b,xo,0.8);
end