function [ res ] = esinve( A )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

res=0;
[n,m]=size(A);
I=eye(n);

if((det(A)~=0) & (n==m) & (A*inv(A)==I))
    disp('SI es invertible');
    res=1;
else
    disp('NO es invertible');
end