function [ y ] = euler(xo, a, b, n)

    %xo condicion inicial
    %a,b intervalo
    %n numero de particiones del intervalo
    %Yn+1=h*F(tn,yn)+yn  n>=0
    
    h=(b-a)/n;
    y=zeros(1,n);
    i=2;
    y(1,1)=xo;

    for to=a:h:b

        y(1,i)=h*funcion(to,y(1,i-1))+y(1,i-1);
        i=i+1;
    end

    %GRAFICA
    
    x=linspace(a,b,n+2);
    
    plot(x,y);
    
    hold on;
end