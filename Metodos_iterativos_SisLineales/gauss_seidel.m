function [ x ] = gauss_seidel( A,b,xo,err )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[n,m]=size(A);

if(esinve(A)==1)

    %disp('entraq al metodo');
    
    D=zeros(n);
    L=zeros(n);
    U=zeros(n);
    
    for(i=1:1:n)
        for(j=1:1:n)
            if(i==j)
               D(i,j)=A(i,j);
            end
            
            if(i>j)
               L(i,j)=A(i,j);
            end
            
            if(i<j)
               U(i,j)=A(i,j);
            end
        end
    end
        
    if(esinve(D+L)==1)
        x=(-(inv(D+L)*(U)*xo))+(inv(D+L)*b);
        disp(x);
        if(((norm((x-xo),inf))/norm(x,inf))>err)
            x=jacobi(A,b,x,err);
        end
    end
        
end
end