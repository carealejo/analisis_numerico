function [ A ] =matriz_p1_t3( )

    A=zeros(16);
    
    for(i=1:1:16)
        for(j=1:1:16)
            if(j==i)
               A(i,j)=4;  
            end
            if(j==i+1 & (i>0 & i<16 & i~=4 & i~=8 & i~=12))
               A(i,j)=-1; 
            end
            if(j==i-1 & (i>1 & i<17 & i~=5 & i~=9 & i~=13))
               A(i,j)=-1; 
            end
            if(j==i+4 & (i>0 & i<13))
               A(i,j)=-1; 
            end
            if(j==i-4 & (i>4 & i<17))
               A(i,j)=-1; 
            end
        end
    end
end

